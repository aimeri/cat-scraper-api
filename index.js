const express = require('express');
const cors = require('cors');
const mongoose = require('mongoose');

const app = express();

const mongoURI = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${
  process.env.DB_HOST
}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`;

const catSchema = new mongoose.Schema({
  Title: String,
  Description: String,
  Price: String,
  Location: String,
  Pictures: [String],
  Link: String,
  Posted: String,
  Emailed: {
    type: Number,
    default: 0
  }
});

const Cat = mongoose.model('cat-scraping', catSchema);

app.use(cors());

app.get('/', (req, res) => {
  if (mongoose.connection.readyState === 0) {
    const config = { useNewUrlParser: true, useFindAndModify: false };
    mongoose.connect(
      mongoURI,
      config
    );
  }
  Cat.find({}, 'Title Description Price Location Pictures Posted Link')
    .lean()
    .exec((err, cats) => res.end(JSON.stringify(cats)));
});

const PORT = process.env.PORT || 5000;
app.listen(PORT, () => {});
